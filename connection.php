<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $db = "calendar";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connect Success";
    } catch (PDOException $e) {
        echo "Connect Failed : " . $e->getMessage();
    }